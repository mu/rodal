# Rodal: A Rust object dumper and loader

[![build status](https://gitlab.anu.edu.au/mu/rodal/badges/master/build.svg)](https://gitlab.anu.edu.au/mu/rodal/commits/master)

Rodal is a Rust crate used by
[Zebu](https://gitlab.anu.edu.au/mu/mu-impl-fast),
to save the VM context and restore it at runtime.
It mainly replaced the previous approach of using the `serde` crate to `serialize/deserialize` the context to `JSON` files.

## Features

The current Rodal implementation provides limited object dumping/loading support,
enough for the current requirements of Zebu.

1. It supports dumping to an assembly file through 
the `AsmDumper<W: Write>` struct in `asm_dumper.rs`.

2. It supports dumping the following data types,
as defined in [`rust_std.rs`](https://gitlab.anu.edu.au/mu/rodal/blob/master/src/rust_std.rs):
   - Primitive data types including:
     - boolean ->`bool`,
     - pointer-sized integers -> `std::isize` and `std::usize`,
     - character -> `std::char`,
     - signed and unsigned 8-, 16-, 32- and 64-bit `integer`,
     - 32- and 64-bit `floating point`, 
   - Atomic `Bool`, `Isize` and `Usize`
   - Zero-sized type -> `PhantomData<T>`,
   - Some reference/pointer types 
     - `Box<T>`, `Box<[T]>` and `Box<str>`
     - `&'a T` and `&'a mut T`,
     - `*const T` and `* mut T`,
     - `AtomicPtr<T>`,

Dumping context to an asm file is essential in both AOT and JIT compilation:

- When JITing, loading the context should be as fast as possible 
to minimize the overhead of JIT compilation at runtime
- When AOTing, loading the context should have minimal overhead on startup time.

## Building

The current implementation of Rodal depends on the `#[global_allocator]` attribute, 
which is available in Rust `1.28.0` onwards.

EOD.
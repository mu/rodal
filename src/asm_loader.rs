// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use super::*;
use std::ptr;

/// loads the address bounds (start and end) for the dumped vm
pub unsafe fn load_asm_bounds(start: Address, end: Address) {
    RODAL_BOUND = Some((start, end));
}
pub unsafe fn load_asm_pointer_move<'a, T>(ptr: *mut T) -> T {
    ptr::read(ptr)
}

pub static mut RODAL_BOUND: Option<(Address, Address)> = None;
